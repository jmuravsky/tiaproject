'use strict';

var Dispatcher = {
  _callbacks: [],
  register: function(callback) {
    this._callbacks.push(callback);
    return this._callbacks.length - 1;
  },
  dispatch: function(payload) {
    console.log("Dispatching", payload);
    for (var i in this._callbacks) {
      this._callbacks[i](payload);
    }
  }
}

module.exports = Dispatcher;