var React = require('react');
var ReactDOM = require('react-dom');
var Dispatcher = require('./dispatcher.js');
var TextStore = require('./textstore.js');

var Word = React.createClass({
  render: function() {
    var cssClass = "Word"
    return <span className={cssClass} onClick={this.handleChange}>{this.props.content}</span>;
  }
});

var DecoratedWord = React.createClass({
  render: function() {
    var cssClass = this.props.types[0];
    if(this.props.typesNo >=2){
      var newTypes = [];
      for (key in this.props.types){
        type = this.props.types[key];
        if (type != cssClass){
          newTypes.push(type);
        }                                            
      }
      var newTypesNo = this.props.typesNo - 1;
      contents = <DecoratedWord key={key} content={this.props.content} types={newTypes} typesNo={newTypesNo}/>;
    }else{
      contents = <Word key={key} content={this.props.content}/> 
    }
    return <span className={cssClass}>{contents}</span>; 
  }
});

var Line = React.createClass({
  render: function() {
    var Words = [];
    for (key in this.props.words){
      word = this.props.words[key];
      Words.push(<DecoratedWord key={key} content={word.content} types={word.types} typesNo={word.typesNo}/>)
    }
    return <p class="Line">{this.state.content}</p>;
  }
}); 
 
var ViewBox = React.createClass({
  render: function() {
    return <div id="View">{this.state.content}</div>;
  }
});
 
var EditBox = React.createClass({
  

  render: function() {
    return <textarea id="Edit">{this.state.content}</textarea>;
  }
});
 
ReactDOM.render(
  <Hello name="Svet" />,
  document.getElementById('container')
);
